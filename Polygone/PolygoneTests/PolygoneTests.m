//
//  PolygoneTests.m
//  PolygoneTests
//
//  Created by Florian BUREL on 03/07/2014.
//  Copyright (c) 2014 florian burel. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Polygone.h"

@interface PolygoneTests : XCTestCase

@end

@implementation PolygoneTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

// test les accessors sur numberOfSides
- (void) testNumberOfSide
{
    Polygone * p = [[Polygone alloc] init];
    
    int testValue = 6;
    
    p.numberOfSide = testValue;
    
    int resultValue = p.numberOfSide;
    
    XCTAssertEqual(testValue, resultValue);
    
}

// On ne doit pas pouvoir definir un nombre de cotés > 12
- (void) testMaxNumberOfSide
{
    Polygone * p = [Polygone new];
    
    int testValue = 13;
    
    p.numberOfSide = testValue;
    
    int resultValue = p.numberOfSide;
    
    XCTAssertNotEqual(testValue, resultValue);

}

// On ne doit pas pouvoir definir un nombre de cotés < 3
- (void) testMinNumberOfSide
{
    Polygone * p = [[Polygone alloc] init];
         int testValue = 2;
    
    [p setNumberOfSide:testValue];
    
    int resultValue = [p numberOfSide];
    
    XCTAssertNotEqual(testValue, resultValue);
    
}

// Lorsque l'on crée un nouveau polygone, son nombre de coté par défaut est 3
- (void) testDefaultNumberOfSide
{
    Polygone * p = [Polygone new];
    
    XCTAssertEqual(3, [p numberOfSide]);
    
}

// test le nom des polygone
- (void) testPolygoneNames
{
    NSArray * names =  @[
                         @"Triangle",
                         @"Quadrilatère",
                         @"Pentagone",
                         @"Hexagone",
                         @"Heptagone",
                         @"Octogone",
                         @"Ennéagone",
                         @"Décagone",
                         @"Hendacagone",
                         @"Dodécagone"
                         ];
    
    Polygone * p = [[Polygone alloc] init];
    
    for (int i = 3  ; i < 13 ; i++)
    {
        [p setNumberOfSide:i];
        NSString * testValue = names[i - 3];
        NSString * resultValue = p.name;
        
        XCTAssertEqualObjects(testValue, resultValue);
    }
}

@end

















