//
//  Polygone.h
//  Polygone
//
//  Created by Florian BUREL on 03/07/2014.
//  Copyright (c) 2014 florian burel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Polygone : NSObject

// Accessors for numberOfSide
@property (nonatomic) int numberOfSide;

// Return the name of the polygone according to its number of side
@property (readonly) NSString * name;


@end
