//
//  PolygoneView.h
//  Polygone
//
//  Created by Florian BUREL on 04/07/2014.
//  Copyright (c) 2014 florian burel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PolygoneView : UIView

@property (nonatomic) int edges;

@end
