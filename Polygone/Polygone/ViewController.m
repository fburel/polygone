//
//  ViewController.m
//  Polygone
//
//  Created by Florian BUREL on 03/07/2014.
//  Copyright (c) 2014 florian burel. All rights reserved.
//


#import "ViewController.h"
#import "Polygone.h"
#import "PolygoneView.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *polygoneTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *polygoneDescriptionLabel;
@property (weak, nonatomic) IBOutlet PolygoneView *polygoneView;
@property (strong, nonatomic) Polygone * polygone;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.polygone = [Polygone new];
    
    [self refresh];
    
}

- (IBAction)changeNumberOfSide:(UISlider *)sender {
    
    self.polygone.numberOfSide = sender.value;
    
    [self refresh];
}


- (void) refresh
{
    self.polygoneTitleLabel.text = self.polygone.name;
    
    self.polygoneDescriptionLabel.text = [NSString stringWithFormat:@"Ce polygone a %d cotés", self.polygone.numberOfSide];
    
    self.polygoneView.edges = self.polygone.numberOfSide;
    
}

@end








