//
//  PolygoneView.m
//  Polygone
//
//  Created by Florian BUREL on 04/07/2014.
//  Copyright (c) 2014 florian burel. All rights reserved.
//

#import "PolygoneView.h"

@implementation PolygoneView



- (void)setEdges:(int)edges
{
    _edges = edges;
    
    [self setNeedsDisplay];
    
}

- (void)drawRect:(CGRect)rect
{
    float mX = self.bounds.size.width / 2.;
    float mY = self.bounds.size.height / 2;
    float radius = MIN(mX, mY);
    float alpha = 2 * M_PI / _edges;
    
    // recuperation du context
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextBeginPath(ctx);
    
    for(int i = 0; i < _edges; i++)
    {
        
        float x = radius * cosf(alpha * i) + mX;
        float y = radius * sinf(alpha * i) + mY;
        
        if(i == 0)
        {
            CGContextMoveToPoint(ctx, x, y);
        }
        else
        {
            CGContextAddLineToPoint(ctx, x, y);
        }
        
    }
    
    CGContextClosePath(ctx);
    
    UIColor * stroke = [UIColor blackColor];
    UIColor * fill = [UIColor redColor];
    
    CGContextSetStrokeColorWithColor(ctx, stroke.CGColor);
    CGContextSetFillColorWithColor(ctx, fill.CGColor);
    
    CGContextDrawPath(ctx, kCGPathFillStroke);
}


@end
