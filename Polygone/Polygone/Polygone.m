//
//  Polygone.m
//  Polygone
//
//  Created by Florian BUREL on 03/07/2014.
//  Copyright (c) 2014 florian burel. All rights reserved.
//

#import "Polygone.h"

@implementation Polygone

- (void)setNumberOfSide:(int)numberOfSide
{
    if(numberOfSide < 3 || numberOfSide > 12)
    {
        // Do nothing
    }
    else
    {
        _numberOfSide = numberOfSide;
    }
}

- (NSString *)name
{
    NSArray * names =  @[
                         @"Triangle",
                         @"Quadrilatère",
                         @"Pentagone",
                         @"Hexagone",
                         @"Heptagone",
                         @"Octogone",
                         @"Ennéagone",
                         @"Décagone",
                         @"Hendacagone",
                         @"Dodécagone"
                         ];

    
    return names[_numberOfSide - 3];
}


- (instancetype)init
{
    self = [super init];
    if (self) {
        _numberOfSide = 3;
    }
    return self;
}


@end








